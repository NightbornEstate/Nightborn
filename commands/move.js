const {
  Command
} = require('discord-akairo');
var Discord = require("discord.js");
var bpf = require("../helpers/build_permission_function");
var util = require("../helpers/util");
class GoCommand extends Command {
  constructor() {
    super('move', {
      aliases: ['move'],
      split: "none",
      userPermissions: bpf(["owner", "techies", "dons", "mods", "intern", "event_orgs"]),
      args: [{
        id: "ch1"
      }, {

	id: "ch2"
	}]
    });
  }
  async exec(message, args) {
    util.log("command." + this.id, "cmd", `Executed by ${message.author.username}#${message.author.discriminator}, with message content ${message.content}`)
    	console.log(args)
	console.log(args.ch1)
	var ch1 = message.guild.channels.get(args.ch1.split(" ")[0])
	var ch2 = message.guild.channels.get(args.ch1.split(" ")[1])
	ch1.members.forEach(m => m.setVoiceChannel(ch2))
  }
}
module.exports = GoCommand;
