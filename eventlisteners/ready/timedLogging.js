module.exports = () => {
  setInterval(() => {
    var dogapi = global.dogapi
    try {
      /**
       * @type {Discord.Guild}
       */
      var nb = client.guilds.get("433009422990835716");
      dogapi.metric.send("server.members", [
        nb.memberCount
      ], {
        type: "gauge"
      }, function (err, results) {
        //
      });
      dogapi.metric.send("server.roles", [
        nb.roles.size
      ], {
        type: "gauge"
      }, function (err, results) {
        //
      });
    } catch (e) {}
  }, 10000)
}
