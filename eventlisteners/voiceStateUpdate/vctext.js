module.exports = async (o, n) => {
  console.log("voiceStateUpdate")
  if (n.voiceChannel && !o.voiceChannel) {
    n.addRole("450403376304357387");
  }
  if (!n.voiceChannel && o.voiceChannel) {
    n.removeRole("450403376304357387");
  }
}
