const {
    Listener
} = require('discord-akairo');
var apiBuilder = require("../api/apiBuilder");
var cLog = require("../helpers/log");
class ReadyListener extends Listener {
    constructor() {
        super('ready', {
            emitter: 'client',
            eventName: 'ready'
        });
    }
    exec() {
        cLog("proccess.main", "info", "Connected to discord!")
        apiBuilder.build(this.client);
    }
}
module.exports = ReadyListener;
